import pandas as pd
import numpy as np

'''
MA - 5, 10
EMA - 5, 10
Momentum - 5, 10 
ACCDIST
ROC
ATR
BBANDS
ADX
MACD


'''


class TA:

    def ROR(self, df, n):
        ROR = pd.Series(df['Close'].pct_change(n), name=f'Return_{n}')
        df = df.join(ROR)
        return df

    # Moving Average
    def MA(self, df, n):
        MA = pd.Series(df['Close'].rolling(n).mean(), name=f'MA_{n}')
        df = df.join(MA)
        return df

    # Exponential Moving Average
    def EMA(self, df, n):
        EMA = pd.Series(df['Close'].ewm(span=n, min_periods=n-1).mean(), name='EMA_'+str(n))
        df = df.join(EMA)
        return df

    # Momentum
    def MOM(self, df, n):
        M = pd.Series(df['Close'].diff(n), name='Momentum_' + str(n))
        df = df.join(M)
        return df

    # Rate of Change
    def ROC(self, df, n):
        M = df['Close'].diff(n - 1)
        N = df['Close'].shift(n - 1)
        ROC = pd.Series(M / N, name='ROC_' + str(n))
        df = df.join(ROC)
        return df

    # Average True Range
    def ATR(self, df, n):
        i = 0
        TR_l = [0]
        while i < df.index[-1]:
            TR = max(df.get_value(i + 1, 'High'), df.get_value(i, 'Close')) - min(df.get_value(i + 1, 'Low'), df.get_value(i, 'Close'))
            TR_l.append(TR)
            i = i + 1
        TR_s = pd.Series(TR_l)
        ATR = pd.Series(TR_s.ewm(span=n, min_periods=n).mean(), name='ATR_'+str(n))
        df = df.join(ATR)
        return df

    # Bollinger Bands
    def BBANDS(self, df, n):
        MA = pd.Series(df['Close'].rolling(n).mean())
        MSD = pd.Series(df['Close'].rolling(n).std())
        b1 = 4 * MSD / MA
        B1 = pd.Series(b1, name='BollingerB_' + str(n))
        df = df.join(B1)
        b2 = (df['Close'] - MA + 2 * MSD) / (4 * MSD)
        B2 = pd.Series(b2, name='Bollinger%b_' + str(n))
        df = df.join(B2)
        return df

    # Pivot Points, Supports and Resistances
    def PPSR(self, df):
        PP = pd.Series((df['High'] + df['Low'] + df['Close']) / 3)
        R1 = pd.Series(2 * PP - df['Low'])
        S1 = pd.Series(2 * PP - df['High'])
        R2 = pd.Series(PP + df['High'] - df['Low'])
        S2 = pd.Series(PP - df['High'] + df['Low'])
        R3 = pd.Series(df['High'] + 2 * (PP - df['Low']))
        S3 = pd.Series(df['Low'] - 2 * (df['High'] - PP))
        psr = {'PP': PP, 'R1': R1, 'S1': S1, 'R2': R2, 'S2': S2, 'R3': R3, 'S3': S3}
        PSR = pd.DataFrame(psr)
        df = df.join(PSR)
        return df

    # Stochastic oscillator %K
    def STOK(self, df):
        SOk = pd.Series((df['Close'] - df['Low']) / (df['High'] - df['Low']), name='SO%k')
        df = df.join(SOk)
        return df

    # Stochastic oscillator %D
    def STO(self, df, n):
        SOk = pd.Series((df['Close'] - df['Low']) / (df['High'] - df['Low']), name='SO%k')
        SOd = pd.Series(SOk.ewm(span=n, min_periods=n-1).mean(), name=f'SO%d{n}')
        df = df.join(SOd)
        return df

    # Trix
    def TRIX(self, df, n):
        EX1 = df['Close'].ewm(span=n, min_periods=n-1).mean()
        EX2 = EX1.ewm(span=n, min_periods=n-1).mean()
        EX3 = EX2.ewm(span=n, min_periods=n-1).mean()

        i = 0
        ROC_l = [0]
        while i + 1 <= df.index[-1]:
            ROC = (EX3[i + 1] - EX3[i]) / EX3[i]
            ROC_l.append(ROC)
            i = i + 1
        Trix = pd.Series(ROC_l, name='Trix_' + str(n))
        df = df.join(Trix)
        return df

    # Average Directional Movement Index
    def ADX(self, df, n, n_ADX):
        i = 0
        UpI = []
        DoI = []
        while i + 1 <= df.index[-1]:
            UpMove = df.get_value(i + 1, 'High') - df.get_value(i, 'High')
            DoMove = df.get_value(i, 'Low') - df.get_value(i + 1, 'Low')
            if UpMove > DoMove and UpMove > 0:
                UpD = UpMove
            else:
                UpD = 0
            UpI.append(UpD)
            if DoMove > UpMove and DoMove > 0:
                DoD = DoMove
            else:
                DoD = 0
            DoI.append(DoD)
            i = i + 1
        i = 0
        TR_l = [0]
        while i < df.index[-1]:
            TR = max(df.get_value(i + 1, 'High'), df.get_value(i, 'Close')) - min(df.get_value(i + 1, 'Low'), df.get_value(i, 'Close'))
            TR_l.append(TR)
            i = i + 1
        TR_s = pd.Series(TR_l)
        ATR = pd.Series(TR_s.ewm(span=n, min_periods=n).mean())
        UpI = pd.Series(UpI)
        DoI = pd.Series(DoI)
        PosDI = pd.Series(UpI.ewm(span=n, min_periods=n-1).mean() / ATR)
        NegDI = pd.Series(DoI.ewm(span=n, min_periods=n-1).mean() / ATR)
        abs_temp = abs(PosDI - NegDI) / (PosDI + NegDI)
        ADX = pd.Series(abs_temp.ewm(span=n_ADX, min_periods=n_ADX - 1).mean(), name=f'ADX_{n}_{n_ADX}')
        df = df.join(ADX)
        return df

    # MACD, MACD Signal and MACD difference
    def MACD(self, df, n_fast, n_slow):
        EMAfast = pd.Series(df['Close'].ewm(span=n_fast, min_periods=n_slow-1).mean())
        EMAslow = pd.Series(df['Close'].ewm(span=n_slow, min_periods=n_slow-1).mean())

        MACD = pd.Series(EMAfast - EMAslow, name='MACD_' + str(n_fast) + '_' + str(n_slow))
        MACDsign = pd.Series(MACD.ewm(span=9, min_periods=8).mean(), name=f'MACDsign_{n_fast}_{n_slow}')
        MACDdiff = pd.Series(MACD - MACDsign, name='MACDdiff_' + str(n_fast) + '_' + str(n_slow))
        df = df.join(MACD)
        df = df.join(MACDsign)
        df = df.join(MACDdiff)
        return df

    # Mass Index
    def MassI(self, df):
        Range = df['High'] - df['Low']
        EX1 = Range.ewm(span=9, min_periods=8).mean()
        EX2 = EX1.ewm(span=9, min_periods=8).mean()
        Mass = EX1 / EX2
        MassI = pd.Series(Mass.rolling(25).sum(), name='Mass Index')
        df = df.join(MassI)
        return df

    # Relative Strength Index
    def RSI(self, df, period):
        delta = df['Close'].diff().dropna()
        u = delta * 0
        d = u.copy()
        u[delta > 0] = delta[delta > 0]
        d[delta < 0] = -delta[delta < 0]
        u[u.index[period-1]] = np.mean( u[:period] ) #first value is sum of avg gains
        u = u.drop(u.index[:(period-1)])
        d[d.index[period-1]] = np.mean( d[:period] ) #first value is sum of avg losses
        d = d.drop(d.index[:(period-1)])
        rs = pd.Series(u.ewm(com=period-1, adjust=False).mean() / d.ewm(com=period-1, adjust=False).mean())
        rsi = pd.Series(100 - 100 / (1 + rs), name=f'RSI_{period}')
        df = df.join(rsi)
        return df



    # True Strength Index
    def TSI(self, df, r, s):
        M = pd.Series(df['Close'].diff(1))
        aM = abs(M)
        EMA1 = pd.Series(M.ewm(span=r, min_periods=r-1).mean())
        aEMA1 = pd.Series(aM.ewm(span=r, min_periods=r-1).mean())
        EMA2 = pd.Series(EMA1.ewm(span=s, min_periods=s-1).mean())
        aEMA2 = pd.Series(aEMA1.ewm(span=s, min_periods=s-1).mean())

        TSI = pd.Series(EMA2 / aEMA2, name='TSI_' + str(r) + '_' + str(s))
        df = df.join(TSI)
        return df

    # Accumulation/Distribution
    def ACCDIST(self, df, n):
        ad = ((df['High'] - df['Open']) + (df['Close'] - df['Low'])) / (2 * (df['High'] - df['Low']))
        ad = ad * 100
        AD = pd.Series(ad, name='ADO')
        df = df.join(AD)
        return df

    # Chaikin Oscillator
    def Chaikin(self, df):
        ad = (2 * df['Close'] - df['High'] - df['Low']) / (df['High'] - df['Low']) * 100
        Chaikin = pd.Series(ad.ewm(span=3, min_periods=2).mean() - ad.ewm(span=10, min_periods=9).mean(), name='Chaikin')
        df = df.join(Chaikin)
        return df

    # Money Flow Index and Ratio
    def MFI(self, df, n):
        PP = (df['High'] + df['Low'] + df['Close']) / 3
        i = 0
        PosMF = [0]
        while i < df.index[-1]:
            if PP[i + 1] > PP[i]:
                PosMF.append(PP[i + 1] * df.get_value(i + 1, 'Volume'))
            else:
                PosMF.append(0)
            i = i + 1
        PosMF = pd.Series(PosMF)
        TotMF = PP * df['Volume']
        MFR = pd.Series(PosMF / TotMF)
        MFI = pd.Series(MFR.rolling(n).mean(), name=f'MFI_{n}')
        df = df.join(MFI)
        return df

    # On-balance Volume
    def OBV(self, df, n):
        i = 0
        OBV = [0]
        while i < df.index[-1]:
            if df.get_value(i + 1, 'Close') - df.get_value(i, 'Close') > 0:
                OBV.append(df.get_value(i + 1, 'Volume'))
            if df.get_value(i + 1, 'Close') - df.get_value(i, 'Close') == 0:
                OBV.append(0)
            if df.get_value(i + 1, 'Close') - df.get_value(i, 'Close') < 0:
                OBV.append(-df.get_value(i + 1, 'Volume'))
            i = i + 1
        OBV = pd.Series(OBV)
        OBV_ma = pd.Series(OBV.rolling(n).mean(), name=f'OBV_{n}')
        df = df.join(OBV_ma)
        return df

    # Force Index
    def FORCE(self, df, n):
        F = pd.Series(df['Close'].diff(n) * df['Volume'].diff(n), name='Force_' + str(n))
        df = df.join(F)
        return df

    # Ease of Movement
    def EOM(self, df, n):
        EoM = (df['High'].diff(1) + df['Low'].diff(1)) * (df['High'] - df['Low']) / (2 * df['Volume'])
        Eom_ma = pd.Series(EoM.rolling(n).mean(), name=f'EoM_{n}')

        df = df.join(Eom_ma)
        return df

    # Commodity Channel Index
    def CCI(self, df, n):
        PP = (df['High'] + df['Low'] + df['Close']) / 3
        CCI = pd.Series((PP - PP.rolling(n).mean()) / PP.rolling(n).std(), name=f'CCI_{n}')
        df = df.join(CCI)
        return df

    # Coppock Curve
    def COPP(self, df, n):
        M = df['Close'].diff(int(n * 11 / 10) - 1)
        N = df['Close'].shift(int(n * 11 / 10) - 1)
        ROC1 = M / N
        M = df['Close'].diff(int(n * 14 / 10) - 1)
        N = df['Close'].shift(int(n * 14 / 10) - 1)
        ROC2 = M / N
        ROC_sum = ROC1 + ROC2
        Copp = pd.Series(ROC_sum.ewm(span=n, min_periods=n).mean(), name=f'Copp_{n}' )
        df = df.join(Copp)
        return df



    # Donchian Channel
    def DONCH(self, df, n):
        i = 0
        DC_l = []
        while i < n - 1:
            DC_l.append(0)
            i = i + 1
        i = 0
        while i + n - 1 < df.index[-1]:
            DC = max(df['High'].ix[i:i + n - 1]) - min(df['Low'].ix[i:i + n - 1])
            DC_l.append(DC)
            i = i + 1
        DonCh = pd.Series(DC_l, name='Donchian_' + str(n))
        DonCh = DonCh.shift(n - 1)
        df = df.join(DonCh)
        return df

    # Standard Deviation
    def STDDEV(self, df, n):
        df = df.join(pd.Series(df['Close'].rolling(n).std(), name='STD_' + str(n)))
        return df
