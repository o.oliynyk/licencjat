import sqlite3 as lite
import pandas as pd
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from helpers import select_outperforming
import os

class Streamer:

    def __init__(self):

        self.db = lite.connect('data/stocks_wig.db')
        self.dates = self.get_all_dates()
        self.sim_length = len(self.dates)
        self.current_date_ix = 0
        self.current_data = self.get_current_data()

    def next_day(self):
        self.current_date_ix += 1
        self.current_data = self.get_current_data()

    def get_current_data(self):
        current_date = self.get_current_date()
        data = pd.read_sql(f'select * from data where Date = "{current_date}"', self.db)
        return data

    def get_current_date(self):
        return self.dates[self.current_date_ix]

    def get_all_dates(self):
        dates =  self.db.execute('select distinct Date from data where Date >= "2012-01-01" order by date(Date) asc; ').fetchall()
        return [date[0] for date in dates]



class Classifier:

    def __init__(self):

        self.clf = self.create_classifier()


    def create_classifier(self):
        _db = lite.connect('data/stocks_wig.db')

        training_data = pd.read_sql('select * from data where Date like "%2011%";', _db)
        training_data = self._prepare_learning_data(training_data)
        X_train, y_train = training_data.values()
        clf = tree.DecisionTreeClassifier(max_depth=5)
        clf.fit(X_train, y_train)
        return clf

    def _prepare_learning_data(self, df):
        X = df.drop(['Date', 'Ticker', 'Open', 'High', 'Low', 'Close', 'Volume', 'Daily_Return', 'Class'], axis=1)
        y = df['Class']
        prepared_data = {

            'x': X,
            'y': y
        }
        return prepared_data

    def _prepare_data(self, df):
        X = df.drop(labels=['Date', 'Ticker', 'Open', 'High', 'Low', 'Close', 'Volume', 'Daily_Return', 'Class'], axis=1)
        y = df['Class']
        return (X, y)

    def _predict(self, df):
        data = self._prepare_data(df)[0]
        prediction = self.clf.predict(data)
        return prediction

    def retrain(self, month, year):
        _db = lite.connect('data/stocks_wig.db')
        training_data = pd.read_sql(f'select * from data where Date like "%{year}%"', _db)

        training_data = self._prepare_learning_data(training_data)
        X_train, y_train = training_data.values()
        clf = tree.DecisionTreeClassifier(max_depth=5)
        clf.fit(X_train, y_train)
        self.clf = clf




class Portfolio:

    def __init__(self):

        self.fee = 0.0015

        self.cash = 1000000
        self.size = 5
        self.held_stocks = {}

    def buy(self, stock, price, date):
        budget = self.cash / (self.size - len(self.held_stocks.keys()))
        adj_price = price + (price * self.fee)
        adj_amount = self.max_buy_amount(adj_price, budget)
        self.cash -= adj_price * adj_amount
        self.held_stocks[stock] = adj_amount
        return [date, 'buy', stock, adj_price, adj_amount, adj_amount*adj_price]

    def sell(self, stock, price, date):
        if stock in self.held_stocks.keys():
            amount = self.held_stocks.pop(stock)
            adj_price = price - (price * self.fee)
            self.cash += amount * adj_price
            return [date, 'sell', stock, adj_price, amount, amount*adj_price]

    def max_buy_amount(self, price, budget):
        price = price + (price * self.fee)
        return int(budget / price)

    def initial_portfolio(self, data, date):
        to_buy = select_outperforming(data)
        maximum_positions = self.size - len(self.held_stocks.keys())
        bought = []
        if len(to_buy) > maximum_positions:
            to_buy = to_buy.sample(n=maximum_positions)

        for index, row in to_buy.iterrows():
            bought.append(self.buy(row.Ticker, row.Close, date))
        return bought


class Logger:

    def __init__(self, portfolio, streamer):

        self.portfolio = portfolio
        self.streamer = streamer

        self.action_log = pd.DataFrame(columns=['Date', 'Action', 'Ticker', 'Price', 'Amount', 'Value'])
        self.value_log = pd.DataFrame(columns=['Date', 'Value'])


    def log_value(self, series):
        self.value_log.loc[len(self.value_log)] = series

    def log_action(self, series):
        self.action_log.loc[len(self.action_log)] = series

    def save_logs(self):
        runs = os.listdir('runs/')
        last_run = ''
        try:
            last_run = runs[-1]
        except IndexError as e:
            last_run = 'run_0'

        path = "runs/run_" + str(int(last_run.split('_')[1]) + 1)
        os.mkdir(path)
        self.value_log.to_csv(path + "/value_log.csv")
        self.action_log.to_csv(path + "/action_log.csv")
























