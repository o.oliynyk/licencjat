from DataClasses import Streamer, Classifier, Portfolio, Logger
from helpers import split_held, select_underperforming, select_outperforming
import pandas as pd

class Loop:

    def __init__(self):

        self.streamer = Streamer()
        self.portfolio = Portfolio()
        self.cls = Classifier()
        self.logger = Logger(self.portfolio, self.streamer)
        self._init_portfolio()

    def _init_portfolio(self):
        data = self.streamer.current_data
        data['Prediction'] = self.cls._predict(data)
        logs = self.portfolio.initial_portfolio(data, self.streamer.get_current_date())
        for log in logs:
            self.logger.log_action(log)
        self.streamer.next_day()

    def sell_stocks(self, stocks_df):
        date = self.streamer.get_current_date()
        for index, row in stocks_df.iterrows():
            #print(f'Selling {row.Ticker} on {row.Date} @ {row.Close}')
            log = self.portfolio.sell(row.Ticker, row.Close, date)
            self.logger.log_action(log)

    def buy_stocks(self, stocks_df):
        to_buy = self.portfolio.size - len(self.portfolio.held_stocks.keys())
        date = self.streamer.get_current_date()

        if len(stocks_df) > to_buy:
            stocks_df = stocks_df.sample(n=to_buy)
        for index, row in stocks_df.iterrows():
            #print(f'Buying {row.Ticker} on {row.Date} @ {row.Close}')
            log = self.portfolio.buy(row.Ticker, row.Close, date)
            self.logger.log_action(log)

    def _judge_stocks(self, data):
        held, other = split_held(data, self.portfolio.held_stocks)
        underperforming = select_underperforming(held)
        outperforming = select_outperforming(other)
        if len(underperforming) > 0:
            self.sell_stocks(underperforming)
        need_to_buy = self.portfolio.size - len(self.portfolio.held_stocks.keys())
        if need_to_buy > 0 and (len(outperforming) > 0):
            self.buy_stocks(outperforming)

    def _check_retrain(self, prv_month, prv_year):
        # if today is a different month from yesterday, than we retrain the classifier based on last months data
        current_date = self.streamer.get_current_date()
        month, year = current_date[5:7], current_date[:4]
        if year != prv_year:
            #print(f"Retrained on: {current_date}")
            self.cls.retrain(prv_month, prv_year)

    def log_value(self, data):
        held = self.portfolio.held_stocks
        stocks = split_held(data, held)[0]
        total_value = 0

        for index, row in stocks.iterrows():
            amount = held.get(row.Ticker)
            total_value += amount * row.Close
        total_value += self.portfolio.cash
        #print(self.streamer.get_current_date(), total_value, self.portfolio.cash, self.portfolio.held_stocks.keys())

        log_data = [self.streamer.get_current_date(), total_value]
        self.logger.log_value(log_data)


    def sell_all(self):
        held = split_held(self.streamer.current_data, self.portfolio.held_stocks)[0]
        self.sell_stocks(held)

    def run(self):
        for _ in range(self.streamer.sim_length - 2):
            data = self.streamer.current_data
            data['Prediction'] = self.cls._predict(data)
            self._judge_stocks(data)

            current_date = self.streamer.get_current_date()
            month = current_date[5:7]
            year = current_date[:4]
            self.log_value(data)
            self.streamer.next_day()
            self._check_retrain(month, year)



for i in range(1):
    l = Loop()
    l.run()
    l.logger.save_logs()
    l.sell_all()
    print(f"run {i} {l.portfolio.cash}")