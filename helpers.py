import pandas as pd


def select_outperforming(df):
    return df[df['Prediction'] == 1]

def select_underperforming(df):
    return df[df['Prediction'] == 0]


def split_held(df, held_stocks):
    held = df[df.Ticker.isin(held_stocks.keys())]
    other = df[~df.Ticker.isin(held_stocks.keys())]
    return (held, other)