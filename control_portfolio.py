from DataClasses import Logger, Portfolio, Streamer
from helpers import split_held

streamer = Streamer()
portfolio = Portfolio()
logger = Logger(portfolio, streamer)

portfolio.size = 19
init_data = streamer.current_data

def log_value(data):
    held = portfolio.held_stocks
    stocks = split_held(data, held)[0]
    total_value = 0

    for index, row in stocks.iterrows():
        amount = held.get(row.Ticker)
        total_value += amount * row.Close
    total_value += portfolio.cash
    #print(self.streamer.get_current_date(), total_value, self.portfolio.cash, self.portfolio.held_stocks.keys())

    log_data = [streamer.get_current_date(), total_value]
    logger.log_value(log_data)

for index, row in init_data.iterrows():
    portfolio.buy(row.Ticker, row.Close, streamer.get_current_date())

print(portfolio.cash)
print(portfolio.held_stocks)
log_value(init_data)
streamer.next_day()
for _ in range(streamer.sim_length - 2):
    data = streamer.current_data
    log_value(data)
    streamer.next_day()

logger.save_logs()