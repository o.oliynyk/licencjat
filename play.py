# import talib
# # import sqlite3 as lite
# # import pandas as pd
# #
# #
# # print(talib.MOM.__doc__)

import sqlite3 as lite
import pandas as pd
from sklearn import tree
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix

import graphviz

def run():


    db = lite.connect('data/stocks_wig.db')
    learning_set = pd.read_sql('select * from data where Date like "%2012%"; ', db)
    class_set = learning_set['Class']
    learning_set.drop(['Date', 'Ticker', 'Open', 'High', 'Low', 'Close', 'Volume', 'Daily_Return', 'Class'], axis=1, inplace=True)



    # parameters = {'max_depth': range(3,20), 'min_samples_leaf': range(3,20)}
    # clf = GridSearchCV(tree.DecisionTreeClassifier(), parameters, n_jobs=4)
    # clf = clf.fit(learning_set, class_set)
    # tree_model = clf.best_estimator_
    # print(clf.best_score_, clf.best_params_, tree_model)

    clf = tree.DecisionTreeClassifier(max_depth=5)
    clf.fit(learning_set, class_set)

    data_file = open("data.dot", 'w')
    data = tree.export_graphviz(clf, out_file=data_file, feature_names=learning_set.columns)

    data_file.close()

    test_data =  pd.read_sql('select * from data where Date like "%2013%"; ', db)
    test_class = test_data['Class']
    test_data.drop(['Date', 'Ticker', 'Open', 'High', 'Low', 'Close', 'Volume', 'Daily_Return', 'Class'], axis=1,
                      inplace=True)

    pred = clf.predict(test_data)
    print(confusion_matrix(test_class, pred))
    print(classification_report(test_class, pred))

    #test1.to_sql(name='TESTRUN', con=db, if_exists='replace')


if __name__ == '__main__':
    run()
