import sqlite3 as lite
import pandas as pd
import csv
import talib as ta


def apply_ta(df):

    def _daily_ret(_close):
        return pd.Series(_close.pct_change(1), name='Daily_Return')

    def _classify(row):
        risk_free_daily = 0.00004
        daily_return = row['Daily_Return']
        exces_return = daily_return - risk_free_daily
        if daily_return >= 0:
            return 1
        else:
            return 0
        # if exces_return > risk_free_daily:
        #     return 2
        # elif exces_return < (risk_free_daily * -1):
        #     return 0
        # else:
        #     return 1

    _open = df['Open']
    _high = df['High']
    _low = df['Low']
    _close = df['Close']
    _volume = df['Volume']


    # Daily return
    df['Daily_Return'] = _daily_ret(_close)
    df['Class'] = df.apply(_classify, axis=1)
    df['Class'] = df['Class'].shift(-1)


    # Momentum indicators - ADX, MACD, MFI, MOM, RSI, STOCH, SToCHF, WILLR, ROC
    roc = ta.ROC(_close) # 10 day
    adx = ta.ADX(_high, _low, _close) # 14 day

    macd = ta.MACD(_close) # 12. 26, 9
    mfi = ta.MFI(_high, _low, _close, _volume)
    mom = ta.MOM(_close)
    rsi = ta.RSI(_close)
    stoch = ta.STOCH(_high, _low, _close)
    stochf = ta.STOCHF(_high, _low, _close)
    willr = ta.WILLR(_high, _low, _close)

    df['ADX'] = adx
    df['MACD'] = macd[0]
    df['MACDSIGNAL'] = macd[1]
    df['MFI'] = mfi
    df['MOM'] = mom
    df['RSI'] = rsi
    df['SLOWK'] = stoch[0]
    df['SLOWD'] = stoch[1]
    df['FASTK'] = stochf[0]
    df['FASTD'] = stochf[1]
    df['WILLR'] = willr
    df['ROC'] = roc

    # Volume indicators - Chaikin, OBV
    ad = ta.AD(_high, _low, _close, _volume)
    adosc = ta.ADOSC(_high, _low, _close, _volume) # 3, 10
    obv = ta.OBV(_close, _volume)

    df["ChaikinAD"] = ad
    df["ChaikinOSC"] = adosc
    df["OBV"] = obv

    # Volatility indicators - ATR, TRANGE

    atr = ta.ATR(_high, _low, _close) # 14 tay
    trange = ta.TRANGE(_high, _low, _close)

    df['ATR'] = atr
    df['TRANGE'] = trange

    df.dropna(inplace=True)

    return df




def prepare_data():

    db = lite.connect('data/stocks_single.db')
    db2 = lite.connect('data/stocks_wig.db')

    wig20_stocks = []

    with open('data/wig20.csv') as file:

        reader = csv.reader(file)
        for row in reader:
            wig20_stocks.append(row[0].upper())

    for ticker in wig20_stocks:

        sql = f'''select Date, Ticker, Open, High, Low, Close, Volume from data
                        where Ticker = '{ticker}'
                        and Date >= "2011-01-01"
                        and Date <= "2018-01-01";'''

        df = pd.read_sql(sql, db, parse_dates=['Date'], index_col='Date')
        apply_ta(df)
        df.to_sql('data', db2, if_exists='append')

def create_learning_set():





    db = lite.connect('data/stocks_wig.db')
    df = pd.read_sql("select * from data where Date <= '2012-01-01'", db, parse_dates=['Date']).dropna()
    df['Class'] = df.apply(classify, axis=1)
    df.drop(['index', 'Date', 'Open', 'High', 'Low', 'Close'], inplace=True, axis=1)



    df.to_sql('learning_data', db, if_exists='replace')

prepare_data()
